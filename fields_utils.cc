/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * (part of apt-tools)
 * Copyright (C) 2017-2023 sedret452
 */

#include "fields_utils.hh"

#ifndef LOG_LEVEL
#define LOG_LEVEL lSTD
#endif
#include "logcpp.hh"

// Note: both parser positioning function imitate the functionality of
// LookupParser() as provided in the original apt implementation (as of
// 2023-04-30).
void
parser_position_to_start (ver_it_t const &ver_it, ver_file_it_t &ver_file_it)
{
	ver_file_it = ver_it.FileList();
}
void
seek_parser_position (ver_it_t const &ver_it, ver_file_it_t &ver_file_it)
{
	parser_position_to_start (ver_it, ver_file_it);
	for (; ver_file_it.end() == false; ++ver_file_it) {
		if ((ver_file_it.File()->Flags & pkgCache::Flag::NotSource) == 0)
			break;
	}
	if (ver_file_it.end()) {
		parser_position_to_start (ver_it, ver_file_it);
	}
}

// libapt-pkg requires these two global objects, otherwise it won't work.  See
// the extern declaration in "apt.git/apt-pkg/contrib/configuration.h" and
// "apt.git/apt-pkg/pkgsystem.h"
Configuration* _config {nullptr};
pkgSystem*     _system {nullptr};

PKG_Connector_Err::PKG_Connector_Err()
	: std::runtime_error("PKG Connector error") { }

PKG_Connector* PKG_Connector::pkg_connector {nullptr};

PKG_Connector::PKG_Connector ()
{
	using std::cerr;

	LOG(lDBG) << "APT Package Lib Version: " << pkgLibVersion << "\n";
	LOG(lDBG) << "pkgVersion: " << pkgVersion << "\n";

	if (pkgInitConfig(config) == false) {
		cerr << "Error: Cannot initialize configuration" << "\n";
		throw PKG_Connector_Err();
	}	
	if (pkgInitSystem(config, this->_system) == false) {
		cerr << "Error: Cannot initialize system" << "\n";
		throw PKG_Connector_Err();
	}
	::_config = &config;
	::_system = _system;
	LOG(lDBG) << "Package System Initialized." << "\n";

	LOG(lINFO) << "Initializing query infrastructure ..." << "\n";
	_cache = _cachefile.GetPkgCache();
	if (_cache == nullptr) {
		cerr << "Error: Initialization of Cache" << "\n";
		throw PKG_Connector_Err();
	}
}

pkgCache&
PKG_Connector::get_cache()
{
	if (pkg_connector == nullptr) {
		PKG_Connector::pkg_connector = new PKG_Connector();
	}
	return *(PKG_Connector::pkg_connector->_cache);
}

void
top_10::insert (top_10::pkg_and_field_text_len param)
{
	auto it = std::find_if(vec.begin(),vec.end(),
							   [param](auto cur) {
								   return (param.size > cur.size);
							   }
	);
	if (it != vec.end()) {
		*it = param;
		std::sort(vec.begin(), vec.end(), [](auto a, auto b)
                                  {
                                      return a.size > b.size;
                                  });
	}
}

std::array<top_10::pkg_and_field_text_len, 10>
top_10::get (void)
{
	return vec;
}
