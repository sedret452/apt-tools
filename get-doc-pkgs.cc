/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * get-doc-pkgs.cc: Query the local DEB/APT package cache for
 * documentation-only packages.
 *
 * Background: Some DEB/APT packages split up their in-depth
 * documentation into separate documentation-only packages. This
 * program allows to retrieve a list of installed/installable
 * documentation-only packages -- in relation to the state of the
 * current system's installation.
 *
 * (part of apt-tools)
 * Copyright (C) 2017-2022 sedret452
 */

extern "C" {
#include <unistd.h> // for getopt
#include <getopt.h> // for getopt long
}

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include <apt-pkg/init.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/debsystem.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/pkgrecords.h>
#include <apt-pkg/tagfile.h>

#include <apt-pkg/cacheiterators.h>
Configuration config;
Configuration* _config {&config};
pkgSystem*     _system {0};

#include <iostream>
#include <string>
#include <set>
#include <regex>

#include "utils.hh"

const char* prog_name {"get-doc-pkgs"};
const char* prog_version_str {"0.3.3"};

using std::cout;
using std::cerr;
using std::endl;
using std::string;

void
print_version ()
{	
	LOG(lSTD) << ::prog_name << ", version " << ::prog_version_str << endl;	
}

void
print_usage ()
{
	LOG(lSTD)
		<< ::prog_name << ", version " << ::prog_version_str << endl
		<< "Usage: " << endl
		<< "  " << ::prog_name << " [--help|--version] [-s|-I] [-f <filter>]" << endl
		<< endl
		<< "  General options" << endl
		<< "    -h, --help          Show this help" << endl
		<< "    -v, --version       Show version" << endl
		<< endl
		<< "  Special options" << endl
		<< "    -f <filter>         Select a package filter" << endl
		<< "    --filter <filter>     \"installed\" (default)" << endl
		<< "                          \"installable\"" << endl
		<< "    -I, --infopage-tag  Show only packages providing infopage tag" << endl
		<< "    -s, --summary       Show package summary report" << endl;
}

using pkg_it_t = pkgCache::PkgIterator;
using ver_it_t   = pkgCache::VerIterator;
using pkg_list_t = std::set<string>; // Package list with thin entry type

// Using a dedicated struct to a allow for a comparator that also supports thin
// elements.  See example here:
//   https://en.cppreference.com/w/cpp/container/set/find

// A package list entry type that holds more information than bare data entries
struct fat_entry_t {
	string pkg_name;
	bool   infopages;
};
using pkg_fat_list_t = std::set<fat_entry_t, std::less<>>;

bool operator< (const fat_entry_t& e1, const fat_entry_t& e2 )
{
	return e1.pkg_name < e2.pkg_name;
}
bool operator< (const string& l, const fat_entry_t& e)
{
	return l < e.pkg_name;
}
bool operator< (const fat_entry_t& e, const string& l )
{
	return e.pkg_name < l;
}


void
find_all_installable_doc_packages(bool show_summary, bool infopage_tag_only)
{
	LOG(lDBG) << "Package Version: " << pkgVersion << "\n";
	LOG(lDBG) << "Package Lib Version: " << pkgLibVersion << "\n";
	if (pkgInitConfig(*_config) == false) {
		cerr << "Error: Cannot initialize configuration" << "\n";
		return;
	}	
	if (pkgInitSystem(*_config, _system) == false) {
		cerr << "Error: Cannot initialize system" << "\n";
		return;
	}
	LOG(lINFO) << "Initializing query infrastructure ..." << endl;
	pkgCacheFile cache_file;
	pkgCache* const cache {cache_file.GetPkgCache()};
	if (cache == nullptr) {
		cerr << "Error: Initialization of Cache" << "\n";
		return;
	}
	// Prepare access to all cache records (required for Tags retrieval)
	pkgRecords records(*cache);
	LOG(lINFO) << "Initalization succeeded!" << endl;
			
	// Get lists of all installed packages and prepare lists of
	// installed and installable doc packages
	pkg_fat_list_t available_pkg_set;     // A set of packages that are
	                                      // available for this system
	pkg_list_t     installed_pkg_set;     // A set of packages that are
	                                      // installed
	pkg_list_t     installed_doc_pkg_set; // Set of installed "-doc"
									      // packages
	std::regex re_doc_pkg {"^.*-doc"};
	std::regex re_infopage_tag {"made-of::info"};

	auto installed_counter {0u};
	auto installed_doc_counter {0u};

	using pkg_cache_range_t = range_t<pkgCache::PkgIterator, &pkgCache::PkgBegin, &pkgCache::PkgEnd>;
	for (auto it : pkg_cache_range_t (cache)) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};
			LOG(lDBG) << "OUTER [" <<  pkg_name << "]" << endl;

			// lambda-based predicates to improve readibilty
			auto is_installed = [&]() {return it->CurrentState == pkgCache::State::Installed;};
			auto is_doc_package = [&]() {return std::regex_match(pkg_name, re_doc_pkg);};
			if (is_installed()) {
				// For installed doc packages, here, we can ignore whether or
				// whether not they provide infopages
				available_pkg_set.insert({pkg_name, false}); 
				
				if (is_doc_package()) {
					installed_doc_pkg_set.insert(pkg_name);
					installed_doc_counter += 1u;
				}
				else {
					installed_pkg_set.insert(pkg_name);
					installed_counter += 1u;
				}
			}
			else {
				bool pkg_has_infopages = false;

				// :TODO: [1] How to identify the correct version (for the
				// expected install?)
				// :TODO: [2] Find out what the difference is between the
				// version of the outer loop and the one of the inner loop

				if (is_doc_package()) {
					// Note: using it.CurrentVer() for not-installed packages
					// won't return any information
					for (ver_it_t ver_it {it.VersionList()}; not ver_it.end(); ++ver_it) {
						LOG(lDBG) << "[" <<  pkg_name << "]" << " {is-doc}" << endl;
						for (auto file_it {ver_it.FileList()}; not file_it.end(); ++file_it) {
							auto& parser {records.Lookup(file_it)};
							auto tag_str {parser.RecordField("Tag")}; // can be empty!
							LOG(lDBG) << "  [" <<  pkg_name << "]" << endl;
							LOG(lDBG) << "  TAG: " << tag_str << endl;

							// Just search, don't match the full regex
							if (std::regex_search(tag_str, re_infopage_tag)) {
								pkg_has_infopages = true;
								LOG(lDBG) << "=="
											 << "FOUND available infopage -doc package: "
											 << pkg_name
											 << "=="
											 << endl;
							}
						}
					}
				}
				available_pkg_set.insert({pkg_name, pkg_has_infopages});
			}
		}
	}

	// Check for all installed packages, whether a "-doc" package
	// exists (i.e. installed or installable)
	auto candidates_counter {0u};
	for (auto pkg_name : installed_pkg_set) {
		string doc_pkg_name {pkg_name + "-doc"};
		auto doc_pkg_inst_finder {installed_doc_pkg_set.find(doc_pkg_name)};
		
		// lambda-based predicates to improve readibilty (may degrade performance!)
		auto inst_finder_no_success = [&]() {return doc_pkg_inst_finder == installed_doc_pkg_set.end();};
		if (inst_finder_no_success()) {
			auto doc_pkg_avail_finder {available_pkg_set.find(doc_pkg_name)};
			auto avail_finder_success = [&]() {return doc_pkg_avail_finder != available_pkg_set.end();};
			if (avail_finder_success()) {
				if (not infopage_tag_only) {
					candidates_counter += 1u;
					LOG(lSTD) << doc_pkg_name << endl;
				}
				if (infopage_tag_only and doc_pkg_avail_finder->infopages) {
					LOG(lSTD) << doc_pkg_name << endl;
					candidates_counter += 1u;
				}
			}
		}
	}

	if (show_summary) {
		LOG(lSTD)
			<< "Info: Summary:" << "\n"
			<< "  installed packages:          "
			<< installed_pkg_set.size() << "\n"
			<< "  installed (associated) doc packages:   "
			<< installed_doc_counter << "\n"
			<< "  installable (associated) doc packages"
			<< (infopage_tag_only ? " {infopage-tag}" : "")
			<< ": "
			<< candidates_counter
			<< "\n";
	}
}

// Note: in comparison to the function for installable packages, this function
// will print out as early, as possible---instead of collecting resuls in a
// couple of sets.  Therefore, this function will execute way faster than its
// counter part for "installable" packages.
void
find_all_installed_doc_packages(bool show_summary, bool infopage_tag_only)
{
	LOG(lDBG) << "Package Version: " << pkgVersion << "\n";
	LOG(lDBG) << "Package Lib Version: " << pkgLibVersion << "\n";
	if (pkgInitConfig(*_config) == false) {
		std::cerr << "Error: Cannot initialize configuration" << "\n";
		return;
	}	
	if (pkgInitSystem(*_config, _system) == false) {
		std::cerr << "Error: Cannot initialize system" << "\n";
		return;
	}

	LOG(lINFO) << "Initializing query infrastructure ..." << endl;
	pkgCacheFile cache_file;
	pkgCache* const cache {cache_file.GetPkgCache()};
	if (cache == nullptr) {
		std::cerr << "Error: Initialization of Cache" << "\n";
		return;
	}
	// Prepare access to all cache records (required for Tags retrieval)
	pkgRecords records(*cache);
	LOG(lINFO) << "Initalization succeeded!" << endl;

	// Get list of all installed doc packages and print them directly
	auto installed_doc_counter {0u};
	std::regex re_doc_pkg {"^.*-doc"};
	std::regex re_infopage_tag {"made-of::info"};
	using pkg_cache_range_t = range_t<pkgCache::PkgIterator, &pkgCache::PkgBegin, &pkgCache::PkgEnd>;
	for (auto it : pkg_cache_range_t (cache)) {
		const char* name {it.Name()};
		if (name) {
			std::string pkg_name {name};

			// lambda-based predicates to improve readibilty
			auto is_installed   = [&]() {return it->CurrentState == pkgCache::State::Installed;};
			auto is_doc_package = [&]() {return std::regex_match(pkg_name, re_doc_pkg);};
			if (is_installed() && is_doc_package()) {
				if (not infopage_tag_only) {
					LOG(lSTD) << pkg_name << "\n";
					installed_doc_counter += 1u;
				}
				else {
					bool pkg_has_infopages {false};
					// :TODO: [1] How to identify the correct version (for the
					// expected install?)
					// :TODO: [2] Find out what the difference is between the
					// version of the outer loop and the one of the inner loop

					// Note: only for installed packages "CurrentVer()" can be
					// used.
					for (ver_it_t ver_it {it.CurrentVer()}; not ver_it.end(); ++ver_it) {
						LOG(lDBG) << "[" <<  pkg_name << "]" << " {is-doc}" << endl;
						for (auto file_it {ver_it.FileList()}; not file_it.end(); ++file_it) {
							auto& parser {records.Lookup(file_it)};
							auto tag_str {parser.RecordField("Tag")}; // can be empty!
							LOG(lDBG) << "  [" <<  pkg_name << "]" << endl;
							LOG(lDBG) << "  TAG: " << tag_str << endl;

							// Just search, don't match the full regex
							if (std::regex_search(tag_str, re_infopage_tag)) {
								pkg_has_infopages = true;
								LOG(lDBG) << "=="
											  << "FOUND available infopage -doc package: "
											  << pkg_name
											  << "=="
											  << endl;

							}
						}
					}
					if (pkg_has_infopages) {
						LOG(lSTD) << pkg_name << "\n";
						installed_doc_counter += 1u;
					}
				}
			}
		}
	}

	if (show_summary) {
		LOG(lSTD)
			<< "Info: Summary:" << "\n"
			<< "  installed doc packages"
			<< (infopage_tag_only ? " {infopage-tag}" : "")
			<< ": "
			<< installed_doc_counter << "\n";
	}
}

enum cli_err {
	ok = 0, // "No error, all is fine" (Jedi hand wave)
	unknown_option = 1,
	unknown_filter = 2
};

int
main (int argc, char** argv)
{
	bool show_summary {false};
	bool infopage_tag_only {false};
	enum class Filter : uint8_t {
		installable = 4, installed = 2, unknown = 1
	};
	using Filtermask = uint8_t;
	Filtermask filtermask {0};

	// Use compact names to reduce line length in long opts definition
	#define ARG_N no_argument
	#define ARG_Y required_argument
	#define ARG_O optional_argument

	static char short_opts[] {"+f:hIsv"};
	static struct option long_opts[] =
		{
			{"filter",       ARG_Y, 0, 'f'},
			{"help",         ARG_N, 0, 'h'},
			{"infopage-tag", ARG_N, 0, 'I'},
			{"summary",      ARG_N, 0, 's'},
			{"version",      ARG_N, 0, 'v'},
			{0, 0, 0, 0}
		};
	int option_index {0};
	::opterr = 0;
	bool finished_getopt {false};
	while ( !finished_getopt) {
		int curind {::optind}; // See: https://stackoverflow.com/a/2725408
		int c {getopt_long (argc, argv, short_opts, long_opts, &option_index)};
		switch (c) {
		    case -1:
				finished_getopt = true;
				break;
			case 'h': // help
				print_usage();
				exit (cli_err::ok);
				break;
			case 'f': // filter
				{
					Filter filter {Filter::unknown};
					string filter_str (optarg);
					if (filter_str == "installed") {
						filter = Filter::installed;
					}
					if (filter_str == "installable") {
						filter = Filter::installable;
					}

					if (filter == Filter::unknown) {
						std::cerr << "Unknown filter: -" << filter_str << endl;
						print_usage();
						exit (cli_err::unknown_filter);
					}
					else {
						filtermask |= enum_val(filter);
					}
				}
				break;
			case 'I': // infopage-tag
				infopage_tag_only = true;
				break;
			case 's': // summary
				show_summary = true;
				break;
			case 'v': // version
				print_version();
				exit(cli_err::ok);
				break;
		    case '?': // Handling unknown options
				if (optopt) {
					if (isprint (optopt)) {
						cerr << "Unknown short option: -" << (char) optopt << endl;
					}
					else {
						cerr << "Unknown short option: UNPRINTABLE" << endl;
					}
				}
				else {
					cerr << "Unknown long option: " << argv[curind] << endl;
				}
				print_usage();
				exit (cli_err::unknown_option);
		}
	}

	LOG(lINFO) << ::prog_name << "\n";
	if (filtermask & enum_val(Filter::installable) ) {
		find_all_installable_doc_packages(show_summary, infopage_tag_only);
	}
	else { // default, now filter
		find_all_installed_doc_packages(show_summary, infopage_tag_only);
	}
	return cli_err::ok;
}
