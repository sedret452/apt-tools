/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * get-lib-pkgs-multi-ver.cc: Get a list of all installed DEB packages
 * (libraries only) that are installed in more than one version.
 *
 * (part of apt-tools)
 * Copyright (C) 2017-2022 sedret452
 */

extern "C" {
#include <unistd.h> // for getopt
#include <getopt.h> // for getopt long
}

#include <apt-pkg/init.h>
#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/debsystem.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/cachefile.h>
#include <apt-pkg/pkgrecords.h>

#include <apt-pkg/cacheiterators.h>
Configuration config;
Configuration* _config {&config};
pkgSystem*     _system {0};

#include <iostream>
#include <string>
#include <map>
#include <regex>

#ifndef LOG_LEVEL
#define LOG_LEVEL lDBG
#endif
#include "logcpp.hh"

using std::cout;
using std::endl;
using std::cerr;
using std::string;

const char* prog_name {"get-lib-pkgs-multi-ver"};
const char* prog_version_str {"0.2.3"};

void
print_version ()
{	

	LOG(lSTD) << ::prog_name << ", version " << prog_version_str << endl;	
}

void
print_usage ()
{
	LOG(lSTD)
		<< ::prog_name << ", version " << prog_version_str << endl
		<< "Usage: " << endl
		<< "      " << ::prog_name << " [--help|--version] [-l | -Q]" << endl
		<< endl
		<< "      General options" << endl
		<< "        -h, --help       Show this help" << endl
		<< "        -v, --version    Show version" << endl
		<< endl
		<< "      Special options" << endl
		<< "        -l  results formatted as list (default)" << endl
		<< "        -Q  results formatted as installation string" << endl
		;
}


// :TODO: Unfinished Business
// Show all candidates library packages that *seem* to have multi versions
// installed.
// 
// See: ~/bin/show-lib-pkgs-with-multiversions
//
// A more complex version could indicate candidates that could be
// deleted.  That is, (1) there's a higher number of library package
// existing AND (2) the library packages is completely indipendent.
// That is: when deleting the library package no other dependent
// package will be removed.
void
get_lib_pkgs_duplicates ()
{
	LOG(lDBG) << "Package Version: " << pkgVersion << "\n";
	LOG(lDBG) << "Package Lib Version: " << pkgLibVersion << "\n";
	if (pkgInitConfig(*_config) == false) {
		cerr << "Error: Cannot initialize configuration" << "\n";
		return;
	}	
	if (pkgInitSystem(*_config, _system) == false) {
		cerr << "Error: Cannot initialize system" << "\n";
		return;
	}
	LOG(lINFO) << "Initializing query infrastructure ..." << endl;
	pkgCacheFile CacheFile;
	pkgCache* const cache { CacheFile.GetPkgCache() };
	if (cache == nullptr) {
		cerr << "Error: Initialization of Cache" << "\n";
		return;
	}
	LOG(lINFO) << "Initalization succeeded!" << endl;

	using cache_it_t = pkgCache::PkgIterator;
	using pkg_map_t = std::map<string, pkgCache::Package*>;

	// Get a list of all installed packages
	//   (any packet that fits regex "^lib.*")
	// and store their names into pkg_map_t;
	pkg_map_t lib_pkg_map;
	std::regex re_lib_pkg {"^lib.*"};
	unsigned int installed_lib_pkg_counter {0};
	for (cache_it_t it { cache->PkgBegin() }; not it.end(); it++) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};
			if (it->CurrentState == pkgCache::State::Installed
				&& std::regex_match(pkg_name, re_lib_pkg))
				{
					lib_pkg_map.insert(std::make_pair(pkg_name, it));
					installed_lib_pkg_counter += 1;
				}
		}
	}

	// INTERLUDIUM: print all collected lib packages	
	// Remember, std::map will be already sorted by keys during insert
	// operation
	// for (auto& elem: lib_pkg_map) {
	// 	cout << elem.first << "\n";
	// }

	std::regex re_dup_versions {"(.+?)([-.0-9]+)$"};
	for (auto& elem: lib_pkg_map) {
		LOG(lSTD) << elem.first;
		std::cmatch res;
		if (std::regex_match(elem.first.c_str(), res, re_dup_versions)) {
			LOG(lSTD) << "  " << res[1] << " | " << res[2] << "\n";
		}
		else {
			LOG(lSTD) << "\n";
		}
	}
	

	// :TODO: Step 2: Create main indicator: all lib pkgs are cut from right
	// by removing any postfix numbers, e.g. alternative A:
	//    libncurses5
	//    libncurses6
	// or, alternative B:
	//    libapt-inst1.5
	//    libapt-inst2.0
	// or, alternative C:
	//    libxcb-dri2-0
	//    libxcb-dri3-0
	// or, alternative D:
	//    libusb-0.1-4
	//    libusb-1.0-0

	// :TODO: Do not forget sorting before executing something similar
	// to `uniq -d`
}

void
get_lib_pkgs_duplicates_fast ()
{
	LOG(lDBG) << "Package Version: " << pkgVersion << "\n";
	LOG(lDBG) << "Package Lib Version: " << pkgLibVersion << "\n";
	if (pkgInitConfig(*_config) == false) {
		cerr << "Error: Cannot initialize configuration" << "\n";
		return;
	}	
	if (pkgInitSystem(*_config, _system) == false) {
		cerr << "Error: Cannot initialize system" << "\n";
		return;
	}
	LOG(lINFO) << "Initializing query infrastructure ..." << endl;
	pkgCacheFile CacheFile;
	pkgCache* const cache { CacheFile.GetPkgCache() };
	if (cache == nullptr) {
		cerr << "Error: Initialization of Cache" << "\n";
		return;
	}
	LOG(lINFO) << "Initalization succeeded!" << endl;

	using cache_it_t = pkgCache::PkgIterator;
	struct pkg_map_value_t {
		string base_name;
		string version_str;
		pkgCache::Package* pkg_ptr;

		pkg_map_value_t (string base_name, string version_str
						 , pkgCache::Package* pkg_ptr)
			: base_name {base_name}, version_str{version_str}
			, pkg_ptr {pkg_ptr}
		{};
	};
	using pkg_map_t = std::map<string, pkg_map_value_t>;

	// Get a list of all installed packages that hold a version string
	// at the end of their name, e.g.
	// - Example A:
	//    - libncurses5
	//    - libncurses6
	// - Example B:
	//    libapt-inst1.5
	//    libapt-inst2.0
	// - Example C:
	//    libxcb-dri2-0
	//    libxcb-dri3-0
	// - Example D:
	//    libusb-0.1-4
	//    libusb-1.0-0
	// and store them into a package map.
	pkg_map_t lib_pkg_map;
	std::regex re_lib_pkg {"^(lib.+?)([-.0-9]+)$"};
	unsigned int installed_lib_pkg_counter {0};
	for (cache_it_t it {cache->PkgBegin()}; not it.end(); it++) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};
			std::cmatch re_res;
			if (it->CurrentState == pkgCache::State::Installed
				&& std::regex_match(pkg_name.c_str(), re_res, re_lib_pkg))
				{
					std::string name {re_res[1]};
					std::string ver  {re_res[2]};
					if (ver[0] == '-') {
						pkg_map_value_t val {name, ver.substr(1,ver.size() - 1), it};
						lib_pkg_map.insert(std::make_pair(pkg_name, val));
					}
					else {
						pkg_map_value_t val {name, ver, it};
						lib_pkg_map.insert(std::make_pair(pkg_name, val));
					}
					installed_lib_pkg_counter += 1;
				}
		}
	}

	// INTERLUDIUM: print all collected lib packages	
	// for (auto& elem: lib_pkg_map) {
	// 	auto& name = elem.first;
	// 	auto& base_name = elem.second.base_name;
	// 	auto& version_str = elem.second.version_str;
	// 	cout << name << "  " << base_name << " | " << version_str << "\n";
	// }

	// Identify duplicates and print them to output
	//
	// :TODO: This is awful code, replace this with something more
	// readable (functional programming using zip)!
	using it_t = pkg_map_t::const_iterator;
	{
		it_t cur { lib_pkg_map.cbegin() };
		while (cur != lib_pkg_map.cend() ) {
			bool found_dup {false};
			it_t next {cur};
			++next;
			bool getout {false};
			while (next != lib_pkg_map.end() && not getout) {
				auto& cur_base_name {cur->second.base_name};
				auto& next_base_name {next->second.base_name};
				if (cur_base_name == next_base_name) {
					if (not found_dup) {
						found_dup = true;
						LOG(lSTD) << cur_base_name;
						LOG(lSTD) << "\n  " << cur->second.version_str
								  << " -> " << cur->first;
					}
					LOG(lSTD) << "\n  " << next->second.version_str
							  << " -> " << next->first;
					next++;
				}
				else {
					getout = true;
					if (found_dup) { LOG(lSTD) << "\n"; }
				}
			}
			found_dup = false;
			cur = next;
		}
	}
}

enum cli_err {
	ok = 0, // "No error, all is fine" (Jedi hand wave)
	unknown_option = 1,
	unset_result_format = 2
};

int
main (int argc, char** argv)
{
	enum class Result_FMT {
		list, install_string, unknown
	};
	Result_FMT result_format {Result_FMT::unknown};

	// Use compact names to reduce line length in long opts definition
	#define ARG_N no_argument
	#define ARG_Y required_argument
	#define ARG_O optional_argument

	static char short_opts[] {"+hlQv"};
	static struct option long_opts[] =
		{
			{"help",           ARG_N, 0, 'h'},
			{"list-format",    ARG_Y, 0, 'l'},
			{"install-format", ARG_N, 0, 'Q'},
			{"version",        ARG_N, 0, 'v'},
			{0, 0, 0, 0}
		};
	int opt_index {0};
	::opterr = 0;
	bool getopt_done {false};
	while (not getopt_done) {
		int curind {::optind}; // See: https://stackoverflow.com/a/2725408
		int c {getopt_long (argc, argv, short_opts, long_opts, &opt_index)};
		switch (c) {
		    case -1:
				getopt_done = true;
				break;
			case 'h': // help
				print_usage();
				exit (cli_err::ok);
				break;
			case 'l': // list format
				result_format = Result_FMT::list;
				break;
			case 'Q': // format_install_string
				result_format = Result_FMT::install_string;
				break;
			case 'v': // version
				print_version();
				exit (cli_err::ok);
				break;
		    case '?': // Handling unknown options
				if (optopt) {
					if (isprint (optopt)) {
						cerr << "Unknown short option: -" << (char) optopt << endl;
					}
					else {
						cerr << "Unknown short option: UNPRINTABLE" << endl;
					}
				}
				else {
					cerr << "Unknown long option: " << argv[curind] << endl;
				}
				print_usage();
				exit (cli_err::unknown_option);
		}
	}

	switch (result_format) {
	case Result_FMT::list:
		LOG(lINFO) << ::prog_name << "\n";
		get_lib_pkgs_duplicates_fast ();
		break;
	case Result_FMT::install_string:
		LOG(lINFO) << ::prog_name << "\n";
		get_lib_pkgs_duplicates_fast ();
		break;
	case Result_FMT::unknown:
		print_usage();
		return cli_err::unset_result_format;
		break;
	}

	return cli_err::ok;
}
