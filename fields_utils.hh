/* SPDX-License-Identifier: GPL-2.0-only */
/*
 * (part of apt-tools)
 * Copyright (C) 2017-2023 sedret452
 */

#ifndef __FIELDS_UTILS_HH__
#define __FIELDS_UTILS_HH__

#include <string>
#include <array>
#include <algorithm>

// All fields of a single package (record) are stored inside a char buffer
struct pkg_fields_char_buf_t {
	char const* begin_pos;
	char const* end_pos;
};

#include <apt-pkg/pkgsystem.h>
using ver_it_t      = pkgCache::VerIterator;
using ver_file_it_t = pkgCache::VerFileIterator;

// Note: both parser positioning function imitate the functionality of
// LookupParser() as provided in the original apt implementation (as of
// 2023-04-30).
void parser_position_to_start (ver_it_t const &ver_it, ver_file_it_t &ver_file_it);
void seek_parser_position (ver_it_t const &ver_it, ver_file_it_t &ver_file_it);

#include <apt-pkg/init.h>
#include <apt-pkg/configuration.h>
#include <apt-pkg/cachefile.h>

// libapt-pkg requires these two global objects, otherwise it won't work.  See
// the extern declaration in "apt.git/apt-pkg/contrib/configuration.h" and
// "apt.git/apt-pkg/pkgsystem.h"
extern Configuration* _config;
extern pkgSystem*     _system;

#include <stdexcept>
class PKG_Connector_Err : public std::runtime_error {
public:
    PKG_Connector_Err();
};

/*
 * Provides access to APT's package cache and query infrastructures.
 *
 * Let's ignore Singletons considered evil and put thread safety and that kinda
 * stuff aside, for now.  For the time being, all I want to achieve is to
 * prevent this to be creatable multiple times.  It's still to convuluted, I get
 * that.
 */
class PKG_Connector {
	Configuration config;
	pkgSystem*    _system {nullptr};

	pkgCacheFile _cachefile;
	pkgCache* _cache {nullptr};	

	static PKG_Connector* pkg_connector;;
	
	PKG_Connector ();	
public:

	static pkgCache& get_cache();
};


// Used for debugging:
// top 10 of packages with longest field/tag text
class top_10 {
public:
	typedef struct {
		std::string name = "";
		unsigned long int size = 0;
	} pkg_and_field_text_len;

	std::array<pkg_and_field_text_len, 10> vec {};

	void insert (pkg_and_field_text_len);
	std::array<pkg_and_field_text_len, 10> get (void);
};

#endif //__FIELDS_UTILS_HH__
