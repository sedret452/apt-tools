/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * search-fields-only.cc: Search and list Debian packages (from the APT cache)
 * only regarding certain package fields.
 *
 * I.e.: all packages store a "Package" field/tag but only few packages hold a
 * "Enhances" or "Tag" field.
 *
 * (part of apt-tools)
 * Copyright (C) 2017-2023 sedret452
 */

/*
 * A general note: each of the CLI mode handling functions will call
 * parse_packages_fields_buf(..) and provide one to three callback functions. By
 * this, the way the field data structures are parsed is always the same but the
 * actual logic what to do at specific parts of the parsing process is provided
 * from the caller function.  The main idea is it to reduce code duplication.
 * Sadly, this also reduces readability (IHMO).
 */

extern "C" {
#include <unistd.h> // for getopt
#include <getopt.h> // for getopt long
}

#include <apt-pkg/pkgsystem.h>
#include <apt-pkg/debsystem.h>
#include <apt-pkg/pkgrecords.h>
#include <apt-pkg/cacheiterators.h>

#include <cstdio>
#include <iostream>
#include <string>
#include <map>
#include <regex>
#include <string_view>
#include <set>

#include <algorithm>
#include <functional>
#include <tuple>
#include <type_traits>

#ifndef LOG_LEVEL
#define LOG_LEVEL lSTD
#endif
#include "logcpp.hh"

#include "fields_utils.hh"

using std::cout;
using std::endl;
using std::cerr;
using std::string;

const char* prog_name {"search-fields-only"};
const char* prog_version_str {"0.6.1"};

void
print_version ()
{	
	LOG(lSTD) << ::prog_name << ", version " << prog_version_str << endl;	
}

void
print_usage ()
{
	LOG(lSTD)
		<< ::prog_name << ", version " << prog_version_str << endl
		<< "Usage: " << endl
		<< "      " << ::prog_name << " [--help|--version] |" << endl
		<< "                         [-L -f <field> ] |" << endl
		<< "                         [-Q <value> -f <field> ]" << endl
		<< "                         [-C]" << endl
		<< endl
		<< "      General options" << endl
		<< "        -h, --help       Show this help" << endl
		<< "        -v, --version    Show version" << endl
		<< endl
		<< "      Special options" << endl
		<< "        -C         : Collect all currently available field types " << endl
		<< "        -f <field> : Select given field for list or query mode" << endl
		<< "                     (note: these are exact matches only, hence case matters)" << endl
		<< "        -L         : list all packages that define certain field" << endl
		<< "        -Q <value> : query all packages, holding a field selected by -f" << endl
		<< "                     and try to match the field body for given value" << endl
		;
}

// :TODO:
// "It's an older code, but it checks out"
bool
try_fill_field_body_from_tags_buf (string& field_val
								   , const string field_name
								   , pkg_fields_char_buf_t fields_buf
								   )
{
	// Be aware: All fields/tags of a given package record are encoded similar
	// to email header fields, by using RFC822
	std::regex re_field_line {"^" + field_name + ":[ ]*(.*)$"};
	size_t const buf_len = fields_buf.end_pos - fields_buf.begin_pos;
	std::string_view full_text {fields_buf.begin_pos, buf_len};
	
	// Note: Although it is possible to use `regex_match()` with more
	// feature-rich extensions (like running on multiline input), for my
	// current GCC v10 this creates segfault.  This happens after some time
	// only and seems relate to blowing the stack (I tested by reducing
	// stacksize, which resulted in earlier segfaults, there's your
	// hint).  Hence, lets run default regexes, line-by-line.
	size_t parsed_text {0u};
	bool keep_parsing {true};
	do {
		size_t line_end_pos { full_text.find_first_of ('\n', parsed_text) };
		if (line_end_pos == std::string_view::npos) {
			keep_parsing = false;
			line_end_pos = buf_len;
		}
		std::string_view cur {(full_text.data() + parsed_text), line_end_pos - parsed_text};
		std::cmatch result;
		if ( std::regex_match (std::begin(cur), std::end(cur), result, re_field_line) )
			{
				field_val = result[1];
				return true;
			}
		if (line_end_pos <= buf_len) {
			parsed_text = line_end_pos + 1;
		}
	}
	while (keep_parsing);
	return false;
}

// Expected type of a callback function
using callback_t = std::function<void(std::string_view)>;
// :TODO:
// https://stackoverflow.com/questions/20869010/generating-unique-type-identifier-at-compile-time/20869822#20869822

/*
 * Allows mutliple signatures:
 *
 *  parse_package_fields_buf (pkg_name, fields_buf, ...)
 *
 *  callback 1: field_head_no_multiline_cb
 *  callback 2: field_head_multiline_cba
 *  callback 3: field_body_cb
 *
 */
// Be aware: All fields/tags of a given package record are encoded similar to
// email header fields, by using RFC822.  Some of the fields will spread over
// multiple lines.
//
// Originally, parsing was based on a regex ("^([^:]*):(.*)$", where \1 is
// the head and \2 the body) but this approach was abandoned since there was
// a segfault that seemed unfixable and, additionally, it turns out that
// regexex can be really fucking expensive.  Now, a very much simplified
// approach is used (which works quite well for single thread/seqential
// programs): (A) identfiy field lines, by searching (and separating) by the
// next found newline ("\n").  (B) split up head and tail by finding the
// first (from left) colon (":").
template<typename... _callback_t>
bool
parse_package_fields_buf (std::string pkg_name
						  , pkg_fields_char_buf_t fields_buf
						  , _callback_t... cb)
{
	static_assert ( (std::is_convertible<_callback_t, callback_t>::value && ...), "Types not convertible" );
    constexpr auto num_callbacks = (sizeof...(_callback_t));
    static_assert (num_callbacks >= 0 and num_callbacks <= 3, "Too many callback functions.");
    auto&& cb_tuple = std::forward_as_tuple(cb...); // see: https://stackoverflow.com/a/21566069
	                                                // and https://en.cppreference.com/w/cpp/language/sizeof...

	const size_t buf_len {static_cast<const size_t>(fields_buf.end_pos - fields_buf.begin_pos)};
	std::string_view full_text {fields_buf.begin_pos, buf_len};
	LOG(lFULL) << "pkg: >" << pkg_name << "< *Full Text*:" << "\n"
			   << "  " << full_text
			   << "---------" << "\n";
	size_t parsed_text {0u};
	bool keep_parsing {true};
	// Wow!  Originally, I used a regex that created two downsides:
	//
	// 1. For a package with a rather large field text (a "Depends" field of
	// roughly 78000 characters) this created a segfault, that I was unable
	// to fix.
	//
	// 2. After working around the regex segfault -- by a simplified
	// string.find(..) strategy --, the original runtime of 8 seconds reduced to
	// under 1 (you heard me!).  Turns out, I was creating the regular
	// expression object for each run, completely fresh.  Which is stupid enough
	// but also shouldn't consume that much time, as it did.  
	//
	// BTW. if you want a regular expression in the scope of a specific
	// function AND it's going to be used "tremendously often" (TM) but
	// actually is the same regex for each call: maybe, make a tryout with
	// making it `static`?  Just watch the runtime to drop "a little".
	// I'm going to bed, now, and continue to weep there a little longer.
	do {
		std::string_view field_type {};
		std::string_view field_body {};
		const size_t field_start_pos {parsed_text};
		const size_t field_separator_pos { full_text.find_first_of (':', parsed_text) };
		if (std::string::npos == field_separator_pos) { // Parsing error
			LOG(lERR) << "Field separator expected but not found" << "\n";
			return false;
		}

		field_type = std::string_view { (full_text.data() + field_start_pos)
										, (field_separator_pos - parsed_text) };
		parsed_text += field_type.size() + 1; // the ":" was parsed too

		bool is_multiline {false};
		bool field_complete {false};
		do {
			const size_t line_end_pos { full_text.find_first_of ('\n', parsed_text + 1) };
			if (std::string::npos == line_end_pos) { // Parsing error
				LOG(lERR) << "Line end expected but not found" << "\n";
				return false;
			}
			LOG(lFULL) << "Line end found at: " << line_end_pos << "\n";
			if ((line_end_pos + 1) <= buf_len) {
				std::string_view next_char {(full_text.data() + (line_end_pos + 1)), 1};
				if (next_char == " ") {
					LOG(lDBG) << "Multiline with starting blank for field: " << field_type << "\n";
					is_multiline = true;
					parsed_text = line_end_pos + 1;
				}
				else if (next_char == "\n") {
					LOG(lDBG) << "Double newline found, field parsing ends: " << "\n";					
					parsed_text = line_end_pos + 1;
					field_complete = true;					
					keep_parsing = false;
				}
				else {
					field_complete = true;
					parsed_text = line_end_pos + 1;
				}
			}
			
			const size_t field_body_len {(parsed_text - 1) - field_separator_pos};
			field_body = std::string_view {(full_text.data() + field_separator_pos + 1), field_body_len};
		}
		while (!field_complete);

		if constexpr (num_callbacks > 0) {
			if (not is_multiline) {
				std::get<0>(cb_tuple)(field_type);
			}
			else {
				if constexpr (num_callbacks > 1) {// if field head multiline callback is provided
					std::get<1>(cb_tuple)(field_type);
				}
			}
			if constexpr (num_callbacks == 3) {
				std::get<2>(cb_tuple)(field_body);
			}
		}
			
		LOG(lFULL) << "Field:" << "\n"
				   << "  head: >" << field_type << "<" << "\n"
				   << "  body: >" << field_body << "<" << "\n";
		
	}
	while (keep_parsing);
	
	return true;
}

void
collect_all_field_types ()
{
	pkgCache& cache {PKG_Connector::get_cache()};

	// Prepare access to all cache records (required for Tags retrieval)
	pkgRecords records {cache};
	LOG(lINFO) << "Collecting all availble field types: " << endl;

    unsigned int multi_line_counter {0};

	using cache_it_t = pkgCache::PkgIterator;
	using result_list_t = std::list<string/*field type*/>;
	using result_set_elem_t = std::tuple<string /*field type*/, string /*pkg name*/>;
	struct CMP_set_elem {
		bool operator() (const result_set_elem_t x, const result_set_elem_t y) const
		{
			const auto& x_field_type (std::get<0>(x));
			// const auto& x_pkg_name (std::get<1>(x));
			
			const auto& y_field_type (std::get<0>(y));
			// const auto& y_pkg_name (std::get<1>(y));

			// Well, using '<' seemd counter intuitive to me
			// See: https://stackoverflow.com/a/66438404
			return (x_field_type < y_field_type);
		};
	};
	// using result_set_t = std::set<string>;
	using result_set_t = std::set<result_set_elem_t,
								  CMP_set_elem
								  >;
	result_set_t found_field_types;

	for (cache_it_t it {cache.PkgBegin()}; not it.end(); it++) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};
			for (ver_it_t ver_it {it.VersionList()}; not ver_it.end(); ++ver_it) {
				pkgCache::VerFileIterator ver_file_it;
				char const* pkg_record_begin = nullptr;
				char const* pkg_record_end = nullptr;
				seek_parser_position(ver_it, ver_file_it);
				pkgRecords::Parser& parser = records.Lookup(ver_file_it);
				parser.GetRec(pkg_record_begin, pkg_record_end); // libapt

				pkg_fields_char_buf_t fields_buf {pkg_record_begin, pkg_record_end};
				{
					result_list_t field_type_list;
					unsigned int counter {0};
					unsigned int multiline_counter {0};

					auto field_type_cb = [&field_type_list, &counter](std::string_view field_type)
					{
						field_type_list.push_back (string{field_type});
						counter += 1;
					};
					auto field_type_multiline_cb = [&field_type_list, &counter, &multiline_counter](std::string_view field_type)
					{
						field_type_list.push_back (string{field_type});
						counter += 1;
						multiline_counter += 1;
					};
					parse_package_fields_buf (pkg_name, fields_buf
											  , field_type_cb, field_type_multiline_cb);
					multi_line_counter += multiline_counter;


					for (auto& elem: field_type_list) {
						// :TODO: Switch to move semantics
						result_set_elem_t new_elem {elem, pkg_name};
						found_field_types.insert(new_elem);
					}

					LOG(lDBG) << pkg_name << "\n"
							  << "  " << "[Number of multiline Fields]: " << multiline_counter << "\n"
							  << "  " << "[Number of Fields]: " << counter << "\n"
							  << "  " << "[List size]: " << field_type_list.size() << "\n";
				}
			}
		}
	}
	for (auto& elem: found_field_types) {
		LOG(lSTD) << std::get<0>(elem) << "\n";
		// LOG(lDBG) << " | pkg: " << std::get<1>(elem) << "\n";
	}
	LOG(lINFO) << "Number of multi line fields: " << multi_line_counter << "\n";
}

void
list_packages_holding_field (string field)
{
	pkgCache& cache {PKG_Connector::get_cache()};

	// Prepare access to all cache records (required for Tags retrieval)
	pkgRecords records {cache};
	LOG(lINFO) << "Listing packages holding field: " << field << endl;

	using cache_it_t = pkgCache::PkgIterator;
	unsigned int found_pkg_counter {0u};

	using result_map_t = std::map<string /*package name*/, string /*field body*/>;
	result_map_t result_map;
	for (cache_it_t it {cache.PkgBegin()}; not it.end(); it++) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};

			bool pkg_has_field {false};
			for (ver_it_t ver_it {it.VersionList()}; not ver_it.end(); ++ver_it) {
				pkgCache::VerFileIterator ver_file_it;
				char const* pkg_record_begin = nullptr;
				char const* pkg_record_end = nullptr;
				seek_parser_position(ver_it, ver_file_it);
				pkgRecords::Parser& parser = records.Lookup(ver_file_it);
				parser.GetRec(pkg_record_begin, pkg_record_end); // libapt

				string field_body {}; // stores field body
				pkg_fields_char_buf_t fields_buf {pkg_record_begin, pkg_record_end};

				bool pkg_version_has_field {false};
				auto field_type_cb = [&pkg_version_has_field, &field](std::string_view cur_field_head)
				{
					auto res {field.compare (cur_field_head)};
					if (res == 0) {
						pkg_version_has_field = true;
					}
				};
				parse_package_fields_buf (pkg_name, fields_buf, field_type_cb);

				if (pkg_version_has_field) {
					if (not pkg_has_field) {
						result_map.insert(std::make_pair(pkg_name,
														 field_body));
						LOG(lSTD) << pkg_name << "\n";
						// LOG(lINFO) << "[Pkg] " << pkg_name
						// 		   // << " -> [Field body] " << field_body
						// 		   << "\n";
					}
					pkg_has_field = true;
				}
			}
			if (pkg_has_field) {
				found_pkg_counter += 1u;
			}
		}
	}
	// for (auto& elem: result_map) {
	// 	LOG(lSTD) << elem.first << " -> " << elem.second << "\n";
	// }
	LOG(lINFO) << "Summary: Number of packages holding field: "
			   << found_pkg_counter << endl;
}

void
query_packages_for_field_value (std::string field, std::string query_val)
{
	pkgCache& cache {PKG_Connector::get_cache()};
	pkgRecords records {cache};
	LOG(lINFO) << "Quering fields: " << field << endl;

	using cache_it_t = pkgCache::PkgIterator;
	unsigned int found_field_counter {0u}; // num of packages holding given field
	unsigned int found_query_val_counter {0u}; // num of packages holding given query val

	using result_map_t = std::map<string /*package name*/, string /*field body*/>;
	result_map_t result_map;
	for (cache_it_t it {cache.PkgBegin()}; not it.end(); it++) {
		const char* name {it.Name()};
		if (name) {
			string pkg_name {name};

			bool pkg_has_field {false};
			for (ver_it_t ver_it {it.VersionList()}; not ver_it.end(); ++ver_it) {
				pkgCache::VerFileIterator ver_file_it;
				char const* pkg_record_begin = nullptr;
				char const* pkg_record_end = nullptr;
				seek_parser_position(ver_it, ver_file_it);
				pkgRecords::Parser& parser = records.Lookup(ver_file_it);
				parser.GetRec(pkg_record_begin, pkg_record_end); // libapt

				string field_body {}; // stores field body
				pkg_fields_char_buf_t fields_buf {pkg_record_begin, pkg_record_end};

				bool pkg_version_has_field {false};
				auto field_type_cb = [&pkg_version_has_field, &field](std::string_view cur_field_head)
				{
					auto res {field.compare (cur_field_head)};
					if (res == 0) {
						pkg_version_has_field = true;
						LOG(lDBG) << "pkg has field type!" << "\n";
					}
				};
				auto field_type_cb_multiline = [](std::string_view cur_field_head)
				{
					; // don't do anything
				};

				bool field_body_matches {false};
				auto field_body_cb = [&field_body_matches, &pkg_has_field, &pkg_version_has_field, &query_val](std::string_view cur_field_body)
				{
					if (not pkg_has_field && pkg_version_has_field) {
						cur_field_body.remove_prefix(1); // strip leading blank
						cur_field_body.remove_suffix(1); // strip newline
						
						LOG(lDBG) << "pkg has field type! -> Check body"
								  << "  query_val: >" << query_val << "<" << "\n"
								  << "  field_body: >" << cur_field_body << "<" << "\n"
								  << "\n";

						auto res {query_val.compare (cur_field_body)};
						if (res == 0) {
							field_body_matches = true;
						}
					}
				};
				parse_package_fields_buf (pkg_name, fields_buf
										  , field_type_cb
										  , field_type_cb_multiline
										  , field_body_cb
										  );
				if (pkg_version_has_field) {
					if (field_body_matches) {
						found_query_val_counter += 1u;
						// LOG(lINFO) << "[Pkg] " << pkg_name
						// 		   << " -> [Field body] " << field_body << "\n";
						LOG(lSTD) << pkg_name << "\n";
					}
					pkg_has_field = true;
				}
			}
			if (pkg_has_field) {
				found_field_counter += 1u;
			}
		}
	}
	// for (auto& elem: result_map) {
	// 	LOG(lSTD) << elem.first << " -> " << elem.second << "\n";
	// }
	LOG(lINFO) << "Summary:" << "\n";
	LOG(lINFO) << " Number of packages holding exact queried field value: "
			   << found_query_val_counter << "\n";
	LOG(lINFO) << " Number of all packages holding given field: "
			   << found_field_counter << "\n";
}

enum cli_err {
	ok = 0, // "No error, all is fine" (Jedi hand wave)
	unknown_option = 1,
	unknown_mode = 2
};

int
main (int argc, char** argv)
{
	enum class Prog_Mode {
		collect, list, query, unknown
	};
	Prog_Mode mode {Prog_Mode::unknown};
	std::string field {""};
	std::string query_val {""};

	// Use compact names to reduce line length in long opts definition
	#define ARG_N no_argument
	#define ARG_Y required_argument
	#define ARG_O optional_argument

	static char short_opts[] {"+ChLf:Q:v"};
	static struct option long_opts[] =
		{
			{"collect",   ARG_N, 0, 'C'},
			{"help",      ARG_N, 0, 'h'},
			{"list",      ARG_N, 0, 'L'},
			{"field",     ARG_Y, 0, 'f'},
			{"query",     ARG_Y, 0, 'Q'},
			{"version",   ARG_N, 0, 'v'},
			{0, 0, 0, 0}
		};
	int opt_index {0};
	::opterr = 0;
	bool getopt_done {false};
	while (not getopt_done) {
		int curind {::optind}; // See: https://stackoverflow.com/a/2725408
		int c {getopt_long (argc, argv, short_opts, long_opts, &opt_index)};
		switch (c) {
		    case -1:
				getopt_done = true;
				break;
			case 'C': // collect
				mode = Prog_Mode::collect;
				break;
			case 'h': // help
				print_usage();
				exit (cli_err::ok);
				break;
			case 'L': // list
				mode = Prog_Mode::list;
				break;
			case 'f': // field
				{
					std::string _fieldname (optarg);
					field = _fieldname;
				}
				break;
			case 'Q': // query
				mode = Prog_Mode::query;
				{
					std::string _query_val (optarg);
					query_val = _query_val;
				}
				break;
			case 'v': // version
				print_version();
				exit (cli_err::ok);
				break;
		    case '?': // Handling unknown options
				if (optopt) {
					if (isprint (optopt)) {
						cerr << "Unknown short option: -" << (char) optopt << endl;
					}
					else {
						cerr << "Unknown short option: UNPRINTABLE" << endl;
					}
				}
				else {
					cerr << "Unknown long option: " << argv[curind] << endl;
				}
				print_usage();
				exit (cli_err::unknown_option);
		}
	}
	if (mode == Prog_Mode::unknown) {
		print_usage();
		return cli_err::unknown_mode;
	}

	switch (mode) {
	case Prog_Mode::collect:
		LOG(lINFO) << ::prog_name << "\n";
		collect_all_field_types();
		break;
	case Prog_Mode::list:
		LOG(lINFO) << ::prog_name << "\n";
		// :TODO: Validity check for field name
		list_packages_holding_field (field);
		break;
	case Prog_Mode::query:
		LOG(lINFO) << ::prog_name << "\n";
		// :TODO: Validity check for field name
		// :TODO: Validity check for query_val
		query_packages_for_field_value (field, query_val);
		break;
	case Prog_Mode::unknown:
		print_usage();
		return cli_err::unknown_mode;
		break;
	}

	return cli_err::ok;
}
