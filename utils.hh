/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * utils.hh: Common utilities.
 *
 * (part of apt-tools)
 * Copyright (C) 2017-2022 sedret452
 */

#ifndef __UTILS_HH__
#define __UTILS_HH__

#include <type_traits> // std::underlying_type_t

// Helper for enum class (see Scott Meyers book from 2015)
template<typename E>
constexpr auto
enum_val (E enumerator) noexcept
{
	return static_cast<std::underlying_type_t<E>>(enumerator);
}

#include <apt-pkg/cachefile.h>
#include <apt-pkg/cacheiterators.h>
#include <apt-pkg/pkgcache.h>

/* range_t : A wrapper for 'pkgCache' that allows for range-based for loops.
 *
 * Since 'pkgCache' doesn't provide begin() and end()---as also other missing
 * conditions---it doesn't support range-based for loops.  This template allows
 * to create multiple kinds of wrappers around 'pkgCache' each one allowing to
 * be easily used in a range-based for loop.
 *
 * Note: Dereferencing an iterator of this wrapper, will return an iterator of
 * the underlying type (template parameter base_it_t_).  Most importantly: these
 * are iterators in the sense of libapt-pkg and not in the "regular"
 * sense. Therefore, given following example:
 *
 * >>>
 *  using pkg_cache_range_t = range_t<pkgCache::PkgIterator
 *                                    , &pkgCache::PkgBegin
 *                                    , &pkgCache::PkgEnd>;
 *  for (auto it : pkg_cache_range_t (cache))
 *  { ... }
 * <<<
 *
 * The iterator 'it' will be of type 'pkgCache::PkgIterator' and *not* of type
 * 'range_t::iterator'.
 */
using container_t = pkgCache; // (Yeah, that's not ideal, a using declaration on
							  // global scope but the order template parameters
							  // destroys default arguments. If somebody could
							  // fix this "that'd be great".)

template <typename base_it_t_ // base iterator [e.g. pkgCache::PkgIterator]
		  , base_it_t_ (container_t::*begin_fptr_) (void) // member function pointers to begin and
		  , base_it_t_ (container_t::*end_fptr_) (void)   // end function [e.g. &pkgCache::PkgBegin]
		  >
struct range_t {
	using type = range_t<base_it_t_, begin_fptr_, end_fptr_>;

	static constexpr auto begin_fptr = begin_fptr_; // e.g. &pkgCache::PkgBegin
	static constexpr auto end_fptr   = end_fptr_;   // e.g. &pkgCache::PkgEnd
	
	struct it_t {
		// Original iterator type must support operator !=
		using base_it_t = base_it_t_;
		
		base_it_t& base_it; // the original iterator (used for dereferencing)
		
		explicit it_t (base_it_t& base_it) : base_it{base_it} {}

		base_it_t& operator* () { return base_it; } // Dereferencing returns base iterator
		it_t& operator ++()     { base_it++; return *this; }
		it_t  operator ++(int)  { it_t old {base_it}; base_it++; return old; }
		bool operator != (it_t& other) const { return (base_it != other.base_it); }
	};
	using iterator = type::it_t;

	container_t* c {nullptr};
	typename type::it_t::base_it_t b_; // The pointers to base iterators are
									   // stored too since initialization of
	typename type::it_t::base_it_t e_; // range_t works quirky when base pointer
									   // temporaries are used solely
	typename type::it_t b; // proxy object: *b*egin
	typename type::it_t e; // proxy object: *e*nd

	explicit range_t (container_t* c_)
		: c {c_}, b_{(c->*begin_fptr)()}, e_{(c->*end_fptr)()}
		, b {it_t(b_)}, e {it_t(e_)}
	{}
	range_t () = delete;

	typename type::it_t& begin() { return b; }
	typename type::it_t& end()   { return e; }
};

#endif //__UTILS_HH__
