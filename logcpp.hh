/* SPDX-License-Identifier:  CPOL-1.02 */

#ifndef __LOGCPP_HH__
#define __LOGCPP_HH__

/* logcpp: Simple logging library for C++
 *
 * This library is based on the work of Petru Marginean and based on a
 * version of his blog entry "Logging In C++" on drdobbs.com.  The
 * orignal authors work is licensed via "The Code Project Open
 * License (CPOL) 1.02" (https://www.codeproject.com/info/cpol10.aspx).
 *
 * Copyright (C) 2015 Petru Marginean
 * Copyright (C) 2015-2022 sedret452
 */

#include <sstream>
#include <string>
#include <cstdio>

enum TLogLevel {lERR, lWARN, lSTD, lINFO, lDBG, lFULL};

class Log
{
public:
    Log();
    virtual ~Log();
    std::ostringstream& Get(TLogLevel level = lINFO);
public:
    static TLogLevel& ReportingLevel();
    static std::string ToString(TLogLevel level);
protected:
    std::ostringstream os;
private:
    Log(const Log&);
    Log& operator =(const Log&);
};

inline Log::Log()
{
}

inline std::ostringstream& Log::Get(TLogLevel level)
{
	// os << ToString(level) << ": ";
    os << ToString(level) << (level == lSTD ? "" : ": ");
    os << std::string(level > lFULL ? level - lFULL : 0, '\t');
    return os;
}

inline Log::~Log()
{
    fprintf(stdout, "%s", os.str().c_str());
    fflush(stdout);
}

inline TLogLevel& Log::ReportingLevel()
{
    static TLogLevel reportingLevel = lDBG;
    return reportingLevel;
}

inline std::string Log::ToString(TLogLevel level)
{
	static const char* const buffer[] = {"ERROR", "WARN", "", "INFO", "DEBUG", "DEBUG"};
    return buffer[level];
}

#ifndef LOG_LEVEL
#define LOG_LEVEL lFULL
#endif

#define LOG(level) \
  if (level > LOG_LEVEL) ;\
  else Log().Get(level)

#endif //__LOGCPP_HH__
