PROGS=get-doc-pkgs get-lib-pkgs-multi-ver search-fields-only
MODULES=fields_utils
MODULES_OBJS=$(foreach module, $(MODULES), $(addprefix $(module), .o))

HEADER_MODULES=logcpp utils

SRC_PROGS = $(foreach prog, $(PROGS), $(addprefix $(prog), .cc))
SRC_MODULES = $(foreach module, $(MODULES), $(addprefix $(module), .cc .hh))
SRC_HEADER_MODULES = $(foreach header_module, $(HEADER_MODULES), $(addprefix $(header_module), .hh))

CXX=g++-10
CXXFLAGS=-std=c++17 -O0 -DLIBDPKG_VOLATILE_API -Wall
LDLIBS= -lapt-pkg

CODE_FILES=${SRC_PROGS} ${SRC_MODULS} ${SRC_HEADER_MODULES}

all: $(PROGS) $(MODULE_OBJS)

search-fields-only: search-fields-only.cc fields_utils.o
	$(CXX) $(CXXFLAGS) -o search-fields-only $^ $(LDLIBS)
search-fields-only: .EXTRA_PREREQS = ${SRC_HEADER_MODULES}

fields_utils.o: fields_utils.cc fields_utils.hh
fields_utils.o: .EXTRA_PREREQS = logcpp.hh

.PHONY: clean cscope cscope-file-db clean-cscope clean-cscope-file-db

clean:
	@echo "Cleaning up"
	@for f in $(PROGS) $(MODULES_OBJS) ; do if [ -e "$$f" ] ; then rm $$f ; fi ; done

# Note: the dependency to "cscope-file-db" is left out on purpose,
#       to allow only for manual cscope.files generation
cscope: $(CODE_FILES)
	cscope -bq -k -I/usr/include -I/usr/include/c++/10

clean-cscope:
	@for f in  $(wildcard $(addprefix cscope,.in.out .po.out .out)) ; do rm $$f ; done

cscope-file-db:
	@echo "Creating cscope.files"
	@{ for f in ${CODE_FILES} ; do echo $$f; done } | tee cscope.files

clean-cscope-file-db:
	@for f in $(wildcard cscope.files) ; do rm $$f ; done


